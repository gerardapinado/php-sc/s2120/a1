<?php

class Person {
    private $name;

    function __construct($name){
        $this->name = $name;
    }

    function getDetails(){
        return "Your fullname is $this->name.<br/>";
    }
}

class Developer extends Person {
    
    function __construct($name){
        $this->name = $name;
    }
    function getDetails(){
        return "Your fullname is $this->name and you are a Developer.<br/>";
    }
}

class Engineer extends Person {

    function __construct($name){
        $this->name = $name;
    }
    function getDetails(){
        return "Your fullname is $this->name and you are a Engineer.<br/>";
    }
}