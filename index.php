<?php require_once './code.php' ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S3: Activity</title>
</head>
<body>
    <div style='border: 1px solid black; padding: 20px; margin: 30px; width: 25%'>
        <h1 style='margin-top: 0'>Person</h1>
        <?php 
            $person = new Person("Senku Ishigami");
            echo $person->getDetails();
        ?>
        <h1>Developer</h1>
        <?php 
            $developer = new Developer("John Finch Smith");
            echo $developer->getDetails();
        ?>
        <h1>Engineer</h1>
        <?php 
            $engr = new Engineer("Harold Myers Reese");
            echo $engr->getDetails();
        ?>
    </div>
    
    
</body>
</html>